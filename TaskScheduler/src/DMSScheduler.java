import java.util.ArrayList;


public class DMSScheduler extends SchedulingAlgorithm {

	public DMSScheduler(){
		taskList = new ArrayList<ScheduledTask>();
	}
	
	@Override
	public ScheduledTask nextScheduledTask(int currentTime) {
		ScheduledTask nextTask = null;
		/// Find task with smallest time difference between its deadline and the end of its period
		for (int i = 0; i < taskList.size(); i++) {
			ScheduledTask t = taskList.get(i);
			
			if (nextTask == null) nextTask = t;
			else {
				int currentDi = nextTask.period() - nextTask.computationTime();
				int nextDi = t.period() - t.computationTime(); 
				if (nextDi < currentDi) {

					nextTask = t;
				}	
			}
		}		
		return nextTask;
	}


	@Override
	public boolean isSchedulable() {
		/// Get ordered task list
		ArrayList<ScheduledTask> tasks = orderTasks(taskList);		
			
		/// Get task computation times
		for (int i = 0; i < tasks.size(); i++){
			
			double completion = completionTime(tasks, i);			
			ScheduledTask nextTask = tasks.get(i);
			
			/// Check if the completion test was passed
			if (completion > nextTask.period()) {
				return false;
			}
		}		
		
		return true;
	}
	
	
	
	/**
	 * Checks workload over a specified time period of all tasks of equal or higher priority 
	 * than a given task. 
	 * 
	 * @param tasks		the list of tasks
	 * @param taskNumber	the index of the task being checked
	 * @return the completion time of the given task
	 */
	private double completionTime(ArrayList<ScheduledTask> tasks, int taskNumber){
		double t_previous = 0;
		double t_result = 0;
		
		/// Set t0 to computation time of all tasks with equal or higher priority
		for (int i = 0; i <= taskNumber; i++){
			t_result += (double) tasks.get(i).computationTime();
		}		

		/// Compute workload values until the calculated workload is equal to the previous workload
		while (t_result > t_previous){
			t_previous = t_result;
			t_result = 0;

			/// Compute next workload value
			/// The formula used is: W_i(t)= sum(j=1 to i, computation_j * ceil(t/period_j))
			for (int j = 0; j <= taskNumber; j++){
				ScheduledTask task = tasks.get(j);
				t_result += task.computationTime() * Math.ceil(t_previous/(double) task.period());
			}
			
		}
			
		return t_result;
	}
}
