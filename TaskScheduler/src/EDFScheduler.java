import java.util.ArrayList;


public class EDFScheduler extends SchedulingAlgorithm {
	
	public EDFScheduler(){		
		taskList = new ArrayList<ScheduledTask>();
	}


	@Override
	public ScheduledTask nextScheduledTask(int currentTime) {
		ScheduledTask nextTask = null;
		/// Find task with soonest ending period
		for (int i = 0; i < taskList.size(); i++) {
			ScheduledTask t = taskList.get(i);
			
			if (nextTask == null) nextTask = t;
			else {
				int nextTaskLaxity = nextTask.timeUntilPeriod(currentTime) - nextTask.timeRemaining();
				int thisTaskLaxity = t.timeUntilPeriod(currentTime) - t.timeRemaining(); 
				if ((thisTaskLaxity != 0 ) && (thisTaskLaxity < nextTaskLaxity)) {

					nextTask = t;
				}	
			}
		}		
		return nextTask;
	}

	
	@Override
	public boolean isSchedulable() {
		double sum = 0;
		
		/// Formula is sum(i = 1 to n, computation_i / period_i) <= 1
		for (int i = 0; i < taskList.size(); i++){
			ScheduledTask t = taskList.get(i);
			sum += ((double)t.computationTime() / (double)t.period());
		}
		
		return sum <= 1.0;
	}
	


}
