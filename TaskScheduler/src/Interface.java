
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class Interface extends JFrame{
	private static int NUM_TASKS = 5;
	private ButtonGroup scheduleGroupType;
	private JRadioButton rmsButton;
	private JRadioButton edfButton;
	private JRadioButton dmsButton;
	
	private JButton scheduleButton;
	
	private JLabel cParamLabel;
	private JLabel pParamLabel;
	private JLabel dParamLabel;
	private JLabel task1Label;
	private JLabel task2Label;
	private JLabel task3Label;
	private JLabel task4Label;
	private JLabel task5Label;
	
	private JTextField[][] textFields = new JTextField[3][NUM_TASKS];
	
	private JPanel typePanel;
	private JPanel inputPanel;
	
	private ScheduleGraph graph;
	private SchedulingAlgorithm scheduler;
	private TaskSchedule schedule;
	
	private boolean isSchedulable;
	
	public Interface() {
		setTitle("Scheduler");
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE); 
		this.setMinimumSize(new Dimension(600,400));
		initComponents();
	}
	
	private void initComponents(){
		
		Container contentPane = this.getContentPane();
		contentPane.setLayout(new BorderLayout());
		
		//Type Buttons (on the right)
		rmsButton = new JRadioButton("RMS");
		edfButton = new JRadioButton("EDF");
		dmsButton = new JRadioButton("DMS");
		rmsButton.setSelected(true);
		scheduleButton = new JButton("Schedule");
		scheduleButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				ArrayList<ScheduledTask> toSchedule = new ArrayList<ScheduledTask>();
				
				//Get the task info
				for (int i = 0; i < NUM_TASKS; i++) {
					String c = textFields[0][i].getText();
					String p = textFields[1][i].getText();
					if (c.isEmpty() || p.isEmpty())
						continue;
					toSchedule.add(new ScheduledTask(new Task(Integer.parseInt(c), Integer.parseInt(p)), i+1+""));
				}
				
				//Get the schedule type
				//Init scheduler
				if (rmsButton.isSelected()) {
					scheduler = new RMSScheduler();
				} else if (edfButton.isSelected()) {
					scheduler = new EDFScheduler();
				} else if (dmsButton.isSelected()) {
					scheduler = new DMSScheduler();
				} else {
					return;
				}
				scheduler.addAllTasks(toSchedule);
				schedule  = new TaskSchedule(scheduler, 1, toSchedule);
				
				isSchedulable = scheduler.isSchedulable();
				
				//Update the graph
				graph.update(schedule, isSchedulable);
			}
		});
		
		scheduleGroupType = new ButtonGroup();
		scheduleGroupType.add(rmsButton);
		scheduleGroupType.add(edfButton);
		scheduleGroupType.add(dmsButton);
		//scheduleGroupType.setSelected((ButtonModel) scheduleGroupType.getElements().nextElement(), true);
		
		//Labels for the bottom
		cParamLabel = new JLabel("C");
		pParamLabel = new JLabel("P");
		dParamLabel = new JLabel("D");
		
		task1Label = new JLabel("Task 1");
		task2Label = new JLabel("Task 2");
		task3Label = new JLabel("Task 3");
		task4Label = new JLabel("Task 4");
		task5Label = new JLabel("Task 5");
		
		//Setting the text field for the bottom
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < NUM_TASKS; j++) {
				textFields[i][j] = new JTextField();
			}
		}
		//Buttons on the right
		typePanel = new JPanel();
		typePanel.setLayout(new BoxLayout(typePanel, BoxLayout.Y_AXIS));
		typePanel.add(rmsButton);
		typePanel.add(edfButton);
		typePanel.add(dmsButton);
		typePanel.add(scheduleButton);
		
		//Inputs and Labels on the bottom
		inputPanel = new JPanel(new GridLayout(0,NUM_TASKS + 1));
		inputPanel.add(new JLabel());
		inputPanel.add(task1Label);
		inputPanel.add(task2Label);
		inputPanel.add(task3Label);
		inputPanel.add(task4Label);
		inputPanel.add(task5Label);
		
		inputPanel.add(cParamLabel);
		for (int i = 0; i < NUM_TASKS; i++)
			inputPanel.add(textFields[0][i]);
		
		inputPanel.add(pParamLabel);
		for (int i = 0; i < NUM_TASKS; i++)
			inputPanel.add(textFields[1][i]);
		
		inputPanel.add(dParamLabel);
		for (int i = 0; i < NUM_TASKS; i++)
			inputPanel.add(textFields[2][i]);
		
		//Image on the left
		graph = new ScheduleGraph(400,250, schedule, isSchedulable);
		
		contentPane.add(graph, BorderLayout.CENTER);
		contentPane.add(typePanel, BorderLayout.EAST);
		contentPane.add(inputPanel, BorderLayout.PAGE_END);
	}
	
	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				Interface ex = new Interface();
				ex.pack();
				ex.setVisible(true);
			}
		});
	}
}
