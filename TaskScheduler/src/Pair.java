/**
 * Generic pair class, composed of two elements.
 *
 * @param <T>	Element type of first object parameter
 * @param <E>	Element type of second object parameter
 */
public class Pair <T, E>{

	private T x;
	private E y;
	
	public Pair(T first, E second){
		x = first;
		y = second;
	}

	/**
	 * @return the first element
	 */
	protected T getX() {
		return x;
	}

	/**
	 * @return the second element
	 */
	protected E getY() {
		return y;
	}

	/**
	 * @param x the new first element value
	 */
	protected void setX(T x) {
		this.x = x;
	}

	/**
	 * @param y the new second element value
	 */
	protected void setY(E y) {
		this.y = y;
	}
	
	
	
	
}
