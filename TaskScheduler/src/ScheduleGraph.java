
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Line2D;
import java.util.ArrayList;

import javax.swing.JComponent;

public class ScheduleGraph extends JComponent {
	// the top-left corner of the container
	private static final int BOX_X = 10;
	private static final int BOX_Y = 10;

	private int width;
	private int height;
	private boolean isSchedulable;

	private TaskSchedule schedule;

	/**
	 * The constructor
	 * @param width the width of the container
	 * @param height the height of the container
	 * @param scheduler the scheduler for the set of tasks
	 */
	public ScheduleGraph(int width, int height, TaskSchedule schedule, boolean isSchedulable)
	{
		this.width = width;
		this.height = height;
		this.schedule = schedule;
		this.isSchedulable = isSchedulable;
	}

	/**
	 * draw the container
	 * @param g
	 */
	public void paintComponent(Graphics g)
	{
		Graphics2D g2 = (Graphics2D) g;
		if (!isSchedulable) {
			g2.drawString("Task set is not schedulable.", 125, 50);
		} else if (schedule != null) {

			int scheduleLength = 80;
			schedule.createSchedule(scheduleLength);

			for (int j = 0; j < schedule.getTasks().size(); j++){
				ArrayList<TaskRun> runs = schedule.getTaskRuns(j);
				int taskNum = Integer.parseInt(schedule.getTasks().get(j).name());
				int taskPeriod = schedule.getTasks().get(j).period();
				for (int i = 0; i < runs.size(); i++){
					drawTask(taskNum, runs.get(i).startTime(), runs.get(i).duration(), g2);
				}
				
				//Draw the period lines
				for (int k = 0; k*taskPeriod < scheduleLength/2; k++) {					
					g2.setColor(Color.black);
					g2.draw(new Line2D.Double(BOX_X + taskPeriod*10*k, BOX_Y + (taskNum-1)*50, BOX_X + taskPeriod*10*k, BOX_Y + taskNum*50));
				}
			}
		}
		
		//Draw the box
		g2.setColor(Color.black);
		g2.draw(new Rectangle(BOX_X, BOX_Y, width, height));

		//Draw the horizontal lines of the graph
		Line2D.Double seg1 = new Line2D.Double(BOX_X, BOX_Y + 50, BOX_X + width, BOX_Y + 50);
		Line2D.Double seg2 = new Line2D.Double(BOX_X, BOX_Y + 100, BOX_X + width, BOX_Y + 100);
		Line2D.Double seg3 = new Line2D.Double(BOX_X, BOX_Y + 150, BOX_X + width, BOX_Y + 150);
		Line2D.Double seg4 = new Line2D.Double(BOX_X, BOX_Y + 200, BOX_X + width, BOX_Y + 200);
		g2.draw(seg1);
		g2.draw(seg2);
		g2.draw(seg3);
		g2.draw(seg4);
	}
	private void drawTask(int taskNum, int start, int length, Graphics2D g2) {
		//sets the color of the rectangle to draw
		switch(taskNum){
		case 1:
			g2.setColor(Color.red);
			break;
		case 2:
			g2.setColor(Color.blue);
			break;
		case 3:
			g2.setColor(Color.green);
			break;
		case 4:
			g2.setColor(Color.orange);
			break;
		case 5:
			g2.setColor(Color.gray);
			break;
		}

		int end = length *10;

		//check if it will fit on screen
		if (BOX_X + start*10 + end > BOX_X + width) {
			return;
		}

		//draw the rectangle
		g2.fillRect(BOX_X + start*10, BOX_Y + (taskNum-1)*50 + 10, end, 40);
		g2.setColor(Color.black);
		g2.draw(new Rectangle(BOX_X + start*10, BOX_Y + (taskNum-1)*50 + 10, end, 40));
	}
	/**
	 * update the container.
	 * @param schedule 
	 */  
	public void update(TaskSchedule schedule, boolean isSchedulable)
	{
		this.schedule = schedule;
		this.isSchedulable = isSchedulable;
		repaint();      
	}
}