import java.util.ArrayList;


public class ScheduleTester {

	public static void main(String[] args) {

		ArrayList<ScheduledTask> toSchedule = new ArrayList<ScheduledTask>();
		
		int ci = 3;
		int period = 5;
		Task t1 = new Task(ci, period);
		
		ci = 2;
		period = 10;
		Task t2 = new Task(ci, period);
		
		ci = 1;
		period = 5;
		Task t3 = new Task(ci, period);
		
		ScheduledTask st1 = new ScheduledTask(t1, "First Task");
		toSchedule.add(st1);
		ScheduledTask st2 = new ScheduledTask(t2, "Second Task");
		toSchedule.add(st2);
		ScheduledTask st3 = new ScheduledTask(t3, "Third Task");
		toSchedule.add(st3);
		
		EDFScheduler edf = new EDFScheduler();
		RMSScheduler rms = new RMSScheduler();
		
		int interval = 1;
		
		TaskSchedule schedule = new TaskSchedule(edf, interval, toSchedule);
		edf.addAllTasks(toSchedule);
		if (scheduable(edf)){
			System.out.println("Schedule can be created!");;
		} else {
			System.out.println("Schedule can NOT be created!");;
		}
		
	}

	public static void printSchedule(TaskSchedule schedule){
		int scheduleLength = 35;
		int numScheduled = schedule.createSchedule(scheduleLength);
		
		System.out.println("Total tasks scheduled: " + numScheduled);
		for (int j = 0; j < schedule.getTasks().size(); j++){
			ArrayList<TaskRun> runs = schedule.getTaskRuns(j);
			String taskName = schedule.getTasks().get(j).name();
			
			for (int i = 0; i < runs.size(); i++){
				
				System.out.print(taskName + ", Run " + (i + 1) + " - Start: " + runs.get(i).startTime() + " ");
				System.out.println(" Duration: " + runs.get(i).duration());
			}			
		}
	}
	
	public static void printSortedList(SchedulingAlgorithm algorithm, ArrayList<ScheduledTask> tasks){
		
		int num = 0;
		for (ScheduledTask t : algorithm.orderTasks(tasks)){
			num += 1;
			String taskName = t.name();
				
			System.out.print(num + " " + taskName);
			System.out.println(", Time until deadline: " + t.timeUntilPeriod(0));
						
		}
	}
	
	public static boolean scheduable(SchedulingAlgorithm algorithm){
		return algorithm.isSchedulable();
	}
}
