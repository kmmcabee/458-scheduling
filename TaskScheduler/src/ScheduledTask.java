import java.util.ArrayList;

/**
 * Class representing a task that has been scheduled and executed.
 * This class is used to keep track of each instance a specific task
 * is executed when creating a schedule.
 *
 */
public class ScheduledTask {

	/**
	 * List of instances this task has been executed. These are pair values
	 * representing start times and durations.
	 */
	private ArrayList<TaskRun> runtimes;
	/**
	 * The type of task represented by this object.
	 */
	private Task task;
	/**
	 * Identifier of this particular task.
	 */
	private String name;

	/** 
	 * Time remaining in the current run of the task.
	 */
	private int timeRemaining;
	
	/**
	 * Flag indicating if the task is running.
	 */
	private boolean isRunning;
	
	/**
	 * Creates a new task to be scheduled.
	 * @param t	the type of task that will be scheduled
	 * @param taskName	the identifier of this particular task to be scheduled
	 */
	public ScheduledTask(Task t, String taskName) {
		task = t;
		name = taskName;
		runtimes = new ArrayList<TaskRun>();
		isRunning = false;
		timeRemaining = t.getComputationTime();
	}
	
	/**
	 * Gets the amount of time this task takes to complete.
	 * @return the computation time of this task
	 */
	public int computationTime(){
		return task.getComputationTime();
	}

	/**
	 * Gets the execution period of this task.
	 * @return the period of this task
	 */
	public int period() {
		return task.getPeriod();
	}
	
	/**
	 * Gets the amount of time until the next period will start.
	 * @param currentTime	the current time
	 * @return	time difference between the current time and the start of the next period
	 */
	public int timeUntilPeriod(int currentTime){
		int interval = task.getPeriod();
		int periodNumber = currentTime / interval;
		int nextPeriodTime = interval * (periodNumber + 1);
		
		// Check for beginning of new period
		int timeLeft = nextPeriodTime - currentTime;
		if (timeLeft % interval == 0) return interval;
		return timeLeft;
	}

	/**
	 * Gets the priority level of this task.
	 * @return the task priority
	 */
	public int priority() {
		return task.getPriority();
	}
	
	/**
	 * Gets the identifier of this particular task instance.
	 * @return	name of this task instance
	 */
	public String name(){
		return name;
	}
	/**
	 * Runs the task for the given duration at the given start time. If it is already running, 
	 * the task will continue (i.e. the time will be added to the last run).
	 * @param start	time the task began
	 * @param duration	the amount of time the task will be run
	 * @return	true if a new run time was added, false otherwise
	 */
	public boolean run(int start, int duration){
		
		int actualDuration = 0;
		boolean newRun = false;
		
		actualDuration = (duration > timeRemaining) ? timeRemaining : duration;
		
		if (isRunning) {			
			increaseLastDuration(actualDuration);
		}
		else{		// start a new run			
			
			runtimes.add(new TaskRun(start, actualDuration));
			if (actualDuration < timeRemaining) isRunning = true;
			newRun = true;
		}

		timeRemaining -= actualDuration;

		/// Check bounds of remaining time
		if (timeRemaining < 0) timeRemaining = 0;
		
		if (timeRemaining == 0) {
			startNewPeriod();
		}
		
		return newRun;
	}
	
	/**
	 * Stops the task's current run, because a new period has started.
	 */
	public void startNewPeriod(){
		isRunning = false;
		timeRemaining = task.getComputationTime();
	}
	
	/**
	 * Stops the current running of the task.
	 * @return	true if the task was running, false otherwise
	 */
	public boolean stop(){
		boolean toReturn = isRunning;
		isRunning = false;
		return toReturn;
	}
	
	/**
	 * Adds time to the last instance this task was run. In other words, keep running the
	 * previous task. If the task will complete prior to the addition of the specified time,
	 * the amount of time added will only be enough to complete the task.
	 * @param additionalTime	amount of time to continue the last instance of this task
	 * @return the amount of time the task was actually executed
	 */
	private int increaseLastDuration(int additionalTime){
		
		int timeAdded = (additionalTime > timeRemaining) ? timeRemaining : additionalTime;
		
		lastTask().addToDuration(timeAdded);
		
		return timeAdded;
	}
	
	/**
	 * Specifies the amount of time the last scheduled instance of this task has 
	 * remaining to be completed.
	 * @return	the amount of time until the last scheduled instance of the task completes
	 */
	public int timeRemaining(){
		return timeRemaining;
	}
	
	/**
	 * Specifies if the task is currently in a running state (has not finished or been stopped).
	 * @return	true if the task is currently running, false otherwise
	 */
	public boolean isRunning(){
		return isRunning;
	}
	
	/**
	 * 
	 * @return	the last scheduled instance of this task
	 */
	private TaskRun lastTask(){
		return runtimes.get(runtimes.size() - 1);
	}
	
	/**
	 * Gets the list of start times and durations for this task instance.
	 * @return	list of pairs specifying start times and durations
	 */
	public ArrayList<TaskRun> runtimes(){
		return runtimes;
	}
	
	/**
	 * Determines if this scheduled task is equal to another. This compares the type of task being
	 * scheduled, and the name of the task.
	 * @param toCompare	scheduled task to compare this to
	 * @return	true if conditions mentioned in description are true, false otherwise
	 */
	public boolean equals(ScheduledTask toCompare){
		/// Check type of task
		if (!toCompare.task.equals(this.task)) return false;
		/// Check name of task
		if (!toCompare.name.equals(this.name)) return false;
		
		return true;
	}
}
