import java.util.ArrayList;


/**
 * Defines a scheduling algorithm. Each subclass of this type will be able to 
 * determine which task in a run queue should be run next.
 *
 */
public abstract class SchedulingAlgorithm {


	/**
	 * list of tasks in the run queue
	 */
	protected ArrayList<ScheduledTask> taskList;
	
	
	/**
	 * Adds a task to the run queue.
	 * @param task	task to add
	 */
	public void addTask(ScheduledTask task) {
		taskList.add(task);
	}
	
	/**
	 * Adds all of the tasks from a given list into the run queue. This should be used
	 * when the schedule is initialized.
	 * @param taskList	list of tasks to add to the run queue
	 * @return	true if any tasks were added, false otherwise
	 */
	public boolean addAllTasks(ArrayList<ScheduledTask> tasksToAdd) {
		return taskList.addAll(tasksToAdd);		
	}
	
	/**
	 * Gets a list of the task run queue in order of decreasing priority. This
	 * is used when determining schedulability.
	 * @param tasks	the list of tasks to sort
	 * @return	a list of the run queue in decreasing order
	 */
	public ArrayList<ScheduledTask> orderTasks(ArrayList<ScheduledTask> tasks) {
		
		/// Create new scheduler instance to use for sorting a task list
		EDFScheduler schedule = new EDFScheduler();
		schedule.addAllTasks(tasks);
		/// Create new list of tasks to fill in order
		ArrayList<ScheduledTask> sortedTasks = new ArrayList<ScheduledTask>();
		
		ScheduledTask toAdd;
		while (schedule.hasNextTask()){
			toAdd = schedule.nextScheduledTask(0);
			sortedTasks.add(toAdd);
			schedule.removeTask(toAdd);
		}
		
		return sortedTasks;
	}
	
	/** 
	 * Specifies if there are any tasks in the run queue.
	 * @return	true if there are tasks in the run queue, false otherwise
	 */
	public boolean hasNextTask() {
		return (!taskList.isEmpty());
	}
	
	/**
	 * Gets next task to be scheduled.
	 * @return	task that is next to be scheduled according to the particular 
	 * 			scheduling algorithm
	 * @param currentTime 	the current time within the schedule
	 */
	public abstract ScheduledTask nextScheduledTask(int currentTime);
	
	/**
	 * Removes a task from the run queue. This should be done when the task is complete
	 * and the task's period is not yet elapsed.
	 * @param task	task to remove
	 */
	public void removeTask(ScheduledTask task) {
		taskList.remove(task);
	}
	
	/**
	 * Specifies if the tasks in the run queue can be scheduled.
	 * @return	true if the set of tasks in the run queue pass the schedulability test, false otherwise 
	 */
	public abstract boolean isSchedulable();
	
	
	
}
