/**
 * Basic class representing a particular kind of task.
 *
 */
public class Task {
	/**
	 * Time this task takes to be fully completed.
	 */
	private int computationTime;
	/**
	 * This task needs to be completed once within each interval of this length.
	 */
	private int period;
	/**
	 * Priority level of this task. Lower levels mean higher priority.
	 */
	private int priority;
	
	/**
	 * Creates a new task. The default priority level is -1.
	 * @param ci	computation time of the task
	 * @param pi	period of the task
	 */
	public Task(int ci, int pi){
		computationTime = ci;
		period = pi;
		/// Initialize priority to -1 to show it has not yet been set
		priority = -1;
	}
	
	/**
	 * @return the computation time
	 */
	public int getComputationTime() {
		return computationTime;
	}
	
	/**
	 * @param computationTime the computation time to set
	 */
	public void setComputationTime(int computationTime) {
		this.computationTime = computationTime;
	}
	
	/**
	 * @return the period
	 */
	public int getPeriod() {
		return period;
	}
	
	/**
	 * @param period the period to set
	 */
	public void setPeriod(int period) {
		this.period = period;
	}
	
	/**
	 * @return the priority
	 */
	public int getPriority() {
		return priority;
	}
	
	/**
	 * @param priority the priority to set
	 */
	public void setPriority(int priority) {
		this.priority = priority;
	}
	
	/**
	 * Determines if this task is equal to another. This compares computation time, period, and 
	 * priority level.
	 * @param toCompare	task to compare
	 * @return true if above conditions are met, false otherwise
	 */
	public boolean equals(Task toCompare){
		/// Compare computation times
		if (toCompare.computationTime != this.computationTime) return false;
		/// Compare period
		if (toCompare.period != this.period) return false;
		/// Compare priority
		if (toCompare.priority != this.priority) return false;
		
		return true;
	}
}
