/**
 * Class representing instances of tasks being executed.
 * The first parameter is the starting time, the second specifies
 * the duration the task was executed.
 *
 */
public class TaskRun extends Pair<Integer, Integer> {

	
	/**
	 * Creates a new instance of a scheduled task.
	 * @param startTime	time this task instance began
	 * @param runDuration	the initial amount of time this task instance
	 * 						was executed
	 */
	public TaskRun(Integer startTime, Integer runDuration) {
		super(startTime, runDuration);
	}
	
	/**
	 * Gets the start time of this task instance.
	 * @return	time this task instance was started
	 */
	public int startTime(){
		return super.getX();
	}
	
	/**
	 * Specifies how long this task instance was run.
	 * @return	amount of time this task instance was executed
	 */
	public int duration(){
		return super.getY();
	}
	
	/**
	 * Add time to the duration of this task instance.
	 * @param toAdd	the amount of time to add to this task instance
	 */
	public void addToDuration(int toAdd) {
		int duration = super.getY();
		duration += toAdd;
		super.setY(duration);
	}

	/**
	 * Returns a string representation of this task run instance.
	 */
	public String toString(){
		return ("Start time: " + super.getX() + " Duration: " + super.getY());
	}
}
