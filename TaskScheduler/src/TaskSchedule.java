import java.util.ArrayList;

/**
 * Class that implements a task-scheduler. The initial task list needs to be created 
 * before constructing an instance of this class.
 *
 */
public class TaskSchedule {

	private ArrayList<ScheduledTask> schedule;
	/**
	 * Scheduling algorithm used to determine how tasks are scheduled
	 */
	private SchedulingAlgorithm scheduler;
	/**
	 * Tasks are scheduled at each interval of this duration.
	 */
	private int interval;
	
	
	/**
	 * Creates a new task scheduler.
	 * @param scheduleMethod	algorithm used to schedule tasks
	 * @param timeInterval	interval duration at which tasks will be scheduled
	 * @param taskList	list of tasks that will be scheduled
	 */
	public TaskSchedule(SchedulingAlgorithm scheduleMethod, int timeInterval, ArrayList<ScheduledTask> taskList){
		scheduler = scheduleMethod;
		interval = timeInterval;
		schedule = taskList;
	}
	
	/**
	 * Gets the list of tasks that were scheduled. Each scheduled task has a list of pairs
	 * of start times and durations.
	 * @return
	 */
	public ArrayList<ScheduledTask> getTasks(){
		return schedule;
	}
	
	/**
	 * Gets a list of run times of a given task. Each run time has a start time and a duration.
	 * @param index	the index of the task of which to get run times
	 * @return	a list of run times, composed of start times and durations
	 */
	public ArrayList<TaskRun> getTaskRuns(int index) {
		return schedule.get(index).runtimes();
	}
	
	/**
	 * Creates a task schedule based on the given algorithm and time interval.
	 * @param totalTime	the total time period of the schedule to create
	 * @return	the number of tasks scheduled
	 */
	public int createSchedule(int totalTime){
		
		int numScheduled = 0;
		/// Add all tasks to the run queue.
		//scheduler.addAllTasks(schedule);
		
		ScheduledTask currentTask = null;
		ScheduledTask nextTask = null;
		
		/// Iterate through the schedule at the specified interval
		/// 	until given length is reached.
		for (int currentTime = 0; currentTime < totalTime; currentTime+= interval){
			
			/// Check if tasks need to be added because periods have elapsed
			if (currentTime != 0){
				for (ScheduledTask t : schedule) {
					int timeTilPeriod = t.timeUntilPeriod(currentTime);
					if ((timeTilPeriod <= 0) || (timeTilPeriod == t.period())){
						
						/// If task is a currently running task
						if ((currentTask != null) && (t.equals(currentTask))) {
							/// Remove from run queue since its period interval has elapsed
							scheduler.removeTask(t);
							/// Set currently running task to none
							currentTask = null;
							/// Re-add task because its period has elapsed
							
							scheduler.addTask(t);
							
						}
						else {
							/// Add the task to be scheduled
							scheduler.addTask(t);
						}
						
						t.startNewPeriod();
					}
				}			
			}
			
			/// Get next task to be scheduled
			nextTask = scheduler.nextScheduledTask(currentTime);	
			
			if (nextTask != null){
				// If next task is NOT current task, stop current task
				if ((currentTask != null) && (!nextTask.equals(currentTask))) currentTask.stop();
				
				/// Run next task
				boolean newTaskRun = nextTask.run(currentTime, interval);
				currentTask = nextTask;
				if (newTaskRun) numScheduled += 1;
			}
			
			/// If current task does not need to be run after the current time (is completed)
			if ((currentTask != null) && (!currentTask.isRunning())) {
				scheduler.removeTask(currentTask);
				currentTask = null;
				
			}	
			
			
			
		}
		
		return numScheduled;
	}
	
	public boolean isScheduleable(){
		return true;
	}
}
