
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class Interface extends JFrame{

	private ButtonGroup scheduleGroupType;
	private JRadioButton rmsButton;
	private JRadioButton edfButton;
	private JRadioButton dmsButton;
	
	private JButton scheduleButton;
	
	private JLabel cParamLabel;
	private JLabel pParamLabel;
	private JLabel dParamLabel;
	private JLabel task1Label;
	private JLabel task2Label;
	private JLabel task3Label;
	private JLabel task4Label;
	private JLabel task5Label;
	
	private JTextField[][] textFields = new JTextField[3][5];
	
	private JPanel typePanel;
	private JPanel inputPanel;
	private JPanel graphPanel;
	
	private JPanel task1Panel;
	private JPanel task2Panel;
	private JPanel task3Panel;
	private JPanel task4Panel;
	private JPanel task5Panel;
	
	public Interface() {
		setTitle("Scheduler");
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE); 
		this.setMinimumSize(new Dimension(300,400));
		initComponents();
	}
	
	private void initComponents(){
		
		Container contentPane = this.getContentPane();
		contentPane.setLayout(new BorderLayout());
		
		//Type Buttons (on the right)
		rmsButton = new JRadioButton("RMS");
		edfButton = new JRadioButton("EDF");
		dmsButton = new JRadioButton("DMS");

		scheduleButton = new JButton("Schedule");
		scheduleButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				//TODO: update schedule
			}
		});
		
		scheduleGroupType = new ButtonGroup();
		scheduleGroupType.add(rmsButton);
		scheduleGroupType.add(edfButton);
		scheduleGroupType.add(dmsButton);
		
		//Labels for the bottom
		cParamLabel = new JLabel("C");
		pParamLabel = new JLabel("P");
		dParamLabel = new JLabel("D");
		
		task1Label = new JLabel("Task 1");
		task2Label = new JLabel("Task 2");
		task3Label = new JLabel("Task 3");
		task4Label = new JLabel("Task 4");
		task5Label = new JLabel("Task 5");
		
		//Setting the text field for the bottom
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 5; j++) {
				textFields[i][j] = new JTextField();
			}
		}
		//Buttons on the right
		typePanel = new JPanel();
		typePanel.setLayout(new BoxLayout(typePanel, BoxLayout.Y_AXIS));
		typePanel.add(rmsButton);
		typePanel.add(edfButton);
		typePanel.add(dmsButton);
		typePanel.add(scheduleButton);
		
		//Inputs and Labels on the bottom
		inputPanel = new JPanel(new GridLayout(0,6));
		inputPanel.add(new JLabel());
		inputPanel.add(task1Label);
		inputPanel.add(task2Label);
		inputPanel.add(task3Label);
		inputPanel.add(task4Label);
		inputPanel.add(task5Label);
		
		inputPanel.add(cParamLabel);
		inputPanel.add(textFields[0][0]);
		inputPanel.add(textFields[0][1]);
		inputPanel.add(textFields[0][2]);
		inputPanel.add(textFields[0][3]);
		inputPanel.add(textFields[0][4]);
		
		inputPanel.add(pParamLabel);
		inputPanel.add(textFields[1][0]);
		inputPanel.add(textFields[1][1]);
		inputPanel.add(textFields[1][2]);
		inputPanel.add(textFields[1][3]);
		inputPanel.add(textFields[1][4]);
		
		inputPanel.add(dParamLabel);
		inputPanel.add(textFields[2][0]);
		inputPanel.add(textFields[2][1]);
		inputPanel.add(textFields[2][2]);
		inputPanel.add(textFields[2][3]);
		inputPanel.add(textFields[2][4]);
		
		//Image on the left
		task1Panel = new JPanel();
		task1Panel.setLayout(new BoxLayout(task1Panel, BoxLayout.X_AXIS));
		task1Panel.setBackground(new Color(255,0,0));
		task1Panel.add(Box.createRigidArea(new Dimension(50,50)));
		task2Panel = new JPanel();
		task2Panel.setLayout(new BoxLayout(task2Panel, BoxLayout.X_AXIS));
		task2Panel.setBackground(new Color(0,255,0));
		task2Panel.add(Box.createRigidArea(new Dimension(50,50)));
		task3Panel = new JPanel();
		task3Panel.setLayout(new BoxLayout(task3Panel, BoxLayout.X_AXIS));
		task3Panel.setBackground(new Color(0,0,255));
		task3Panel.add(Box.createRigidArea(new Dimension(50,50)));
		task4Panel = new JPanel();
		task4Panel.setLayout(new BoxLayout(task4Panel, BoxLayout.X_AXIS));
		task4Panel.setBackground(new Color(0,0,0));
		task4Panel.add(Box.createRigidArea(new Dimension(50,50)));
		task5Panel = new JPanel();
		task5Panel.setLayout(new BoxLayout(task5Panel, BoxLayout.X_AXIS));
		task5Panel.setBackground(new Color(255,255,0));
		task5Panel.add(Box.createRigidArea(new Dimension(50,50)));
		
		graphPanel = new JPanel();
		graphPanel.setLayout(new BoxLayout(graphPanel, BoxLayout.Y_AXIS));
		graphPanel.add(task1Panel);
		graphPanel.add(task2Panel);
		graphPanel.add(task3Panel);
		graphPanel.add(task4Panel);
		graphPanel.add(task5Panel);
		
		
		contentPane.add(graphPanel, BorderLayout.LINE_START);
		contentPane.add(Box.createHorizontalGlue(), BorderLayout.CENTER);
		contentPane.add(typePanel, BorderLayout.LINE_END);
		contentPane.add(inputPanel, BorderLayout.PAGE_END);
	}
	
	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				Interface ex = new Interface();
				ex.pack();
				ex.setVisible(true);
			}
		});
	}
}
